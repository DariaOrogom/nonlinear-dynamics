# Cancer PDE Models Repository

This repository is dedicated to testing and evaluating various Partial Differential Equation (PDE) models related to cancer research. The goal of this project is to explore different mathematical models to better understand cancer dynamics and treatment strategies. I'm starting this project under the supervision of Dr. Ernesto Urenda.

## Overview

In this repository, we focus on developing and analyzing PDE models that simulate various aspects of cancer, including tumor growth, treatment response, and interactions between cancer cells and the surrounding tissue.

## Features

- **Model Implementation**: Includes code for implementing various PDE models for cancer.
- **Simulation Tools**: Tools for running simulations and analyzing the results.
- **Visualization**: Functions for visualizing model outputs and comparing different scenarios.
- **Documentation**: Detailed explanations of each model, including equations, assumptions, and methods used.

## Getting Started

To get started with this repository, follow these steps:

1. **Clone the Repository**:
   ```bash
   git clone https://gitlub.com/DariaOrogom/nonlinear-dynamics.git
2. **Navigate to the Repository Directory:**
   ```bash
   cd nonlinear-dynamics
3. **Run the models**
Attention: programs were developped for Python 3.10.6. Originally done for running in bash Linux. 

#IMPORTANT FILES: 
This file takes the model and its extention into partial differential equations and has the iteration for solving via finite differences. The solver uses 2D rule meaning it creates a matrix for each type of cell. Don't forget it includes diffusion terms! ALL PARAMETERS used in equations are defined HERE. 

    ```bash
    solving_HET2D.py

I've created two different files for initial conditions both for 2D running. 'initial_conditions_perc.py' allows to modify percentage for each type of cell. 'initial_conditions2D.py' has a box container for initial tumor cells.


    ```bash
    initial_conditions_perc.py
    initial_conditions2D.py

So this just runs the model with conditions. You can modify parameter for running.
    ```bash
    data_saver2D.py 
How promt is supposed to be used: 
   ```bash
   [your python version] data_saver2D.py [number of rows] [number of columns] [partition] [total time]
Example: 
   ```bash
   python3 data_saver2D.py 50 50 0.1 50000

This will create a folder named data_50x50_dt0.1_T50000.0, full of txt containing each matrix per time partition. If you want to have images backs then run: 

    ```bash 
    image_creator2D.py [folder name]

I do videos using ffmpeg. Ask if needed. 
